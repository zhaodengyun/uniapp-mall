import App from './App'
import {
	myRequest
} from './util/api.js'



// #ifndef VUE3
import Vue from 'vue'
Vue.filter('formate', (date) => {
	const fdate = new Date(date)
	const y = fdate.getFullYear()
	const m = fdate.getMonth().toString().padStart(2, 0)
	const d = fdate.getDay().toString().padStart(2, 0)
	return y + '-' + m + '-' + d
})

Vue.prototype.$myRequest = myRequest

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
