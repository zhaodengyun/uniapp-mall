const BASE_URL = 'http://localhost:8082'
export const myRequest = (options) => {
	return new Promise((resovle, reject) => {
		uni.request({
			url: BASE_URL + options.url,
			method: options.method || "GET",
			success: (res) => {
				if (res.data.status == 0) {
					resovle(res)
				}else{
					uni.showToast({
						title: "请求接口失败"
					})
				}
				

			},
			fail: (err) => {
				uni.showToast({
					title: "请求接口失败"
				})
				reject(err)
			}
		})
	})
}
